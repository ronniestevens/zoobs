# A basic bootstrap theme

A very basic WordPress Theme, to be used to extend directly or with a child-theme.

Because I am developing, and really want to get rid of all the overhead even the simplest bootstrap starter theme has to offer. 

* support for SASS (https://www.elegantthemes.com/blog/tips-tricks/how-to-use-sass-with-wordpress-a-step-by-step-guide
)
* support of bootstrap menu (https://github.com/wp-bootstrap/wp-bootstrap-navwalker)
* based on Bootstrap 4.3.1
* includes jQuery
