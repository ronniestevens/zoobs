<aside class="col-md-4 blog-sidebar">
    <?php if( is_active_sidebar( 'sidebar-1') ) { dynamic_sidebar( 'sidebar-1' );} ?>
    <?php if( is_active_sidebar( 'sidebar-2') ) { dynamic_sidebar( 'sidebar-2' );} ?>
    <?php if( is_active_sidebar( 'sidebar-3') ) { dynamic_sidebar( 'sidebar-3' );} ?>
</aside><!-- /.blog-sidebar -->