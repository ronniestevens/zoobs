        </div><!-- /.row -->
    </main><!-- /.container -->
    <footer class="blog-footer">
        <div class="container">
            <div class="row">
                <?php if( is_active_sidebar( 'footer_one') ) { dynamic_sidebar( 'footer_one' );} ?>
            </div>
        </div>    
    </footer>
<?php wp_footer(); ?>
</body>
</html>
