<?php

/* enqueue styles */
function zoobs_enqueue_styles() {
    wp_register_style('bootstrap', get_template_directory_uri() . '/bootstrap/css/bootstrap.min.css' );
    $dependencies = array('bootstrap');
    wp_enqueue_style( 'zoobs-style', get_stylesheet_uri(), $dependencies ); 
    wp_enqueue_style( 'blog-css', get_template_directory_uri().'/css/blog.css');
}
add_action( 'wp_enqueue_scripts', 'zoobs_enqueue_styles' );

/* enqueue scripts */
function zoobs_enqueue_scripts() {
    $dependencies = array('jquery');
    wp_enqueue_script('zoobs-script', get_template_directory_uri().'/bootstrap/js/bootstrap.bundle.min.js', $dependencies, '4.3.1', true );
}
add_action( 'wp_enqueue_scripts', 'zoobs_enqueue_scripts' );

/* title tag support */
function zoobs_wp_setup() {
    add_theme_support( 'title-tag' );
}
add_action( 'after_setup_theme', 'zoobs_wp_setup' );

/* register menu */
function zoobs_register_menu() {
    register_nav_menu('header-menu', __( 'Header Menu' ));
}
add_action( 'init', 'zoobs_register_menu' );

/* base widgets */
function zoobs_widgets() {
    
    register_sidebar( array(
        'name'          => 'Footer - Copyright Text',
        'id'            => 'footer_one',
        'before_widget' => '<div class="footer_copyright_text">',
        'after_widget'  => '</div>',
        'before_title'  => '<h4>',
        'after_title'   => '</h4>',
    ));

    register_sidebar ( array (
        'name'          => 'Sidebar - First',
        'id'            => 'sidebar-1',
        'before_widget' => '<div class="p-4 mb-3 bg-light rounded">',
        'after_widget'  => '</div>',
        'before_title'  => '<h4>',
        'after_title'   => '</h4>',
    ));

    register_sidebar ( array (
        'name'          => 'Sidebar - Second',
        'id'            => 'sidebar-2',
        'before_widget' => '<div class="p-4">',
        'after_widget'  => '</div>',
        'before_title'  => '<h4>',
        'after_title'   => '</h4>',
    ));

    register_sidebar ( array (
        'name'          => 'Sidebar - Last',
        'id'            => 'sidebar-3',
        'before_widget' => '<div class="p-4">',
        'after_widget'  => '</div>',
        'before_title'  => '<h4>',
        'after_title'   => '</h4>',
    ));
}
add_action( 'widgets_init', 'zoobs_widgets' );

/**
 * Register Custom Navigation Walker
 */
function register_navwalker(){
	require_once get_template_directory() . '/class-wp-bootstrap-navwalker.php';
}
add_action( 'after_setup_theme', 'register_navwalker' );

register_nav_menus( array(
    'primary' => __( 'Primary Menu', 'zoobs' ),
) );
?>